#!/usr/bin/env bash

set -eu

ls -al dist/
twine upload "dist/${PACKAGE_NAME//-/_}-${ACTUAL_VERSION}"-*.whl

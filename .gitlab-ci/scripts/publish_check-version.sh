#!/usr/bin/env bash

set -eu

echo "Actual version: ${ACTUAL_VERSION}"

if (python3.7 -m pip search  --no-color "${PACKAGE_NAME}" | grep -F -- "${PACKAGE_NAME}" | grep -F -- "${ACTUAL_VERSION}")
then
    echo "Package version ${ACTUAL_VERSION} already deployed" >&2
    exit 1
fi

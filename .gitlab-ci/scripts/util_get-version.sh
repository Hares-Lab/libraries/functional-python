#!/usr/bin/env bash

set -eu

cat "$1" \
 | grep -F "__version__" \
 | grep -P "^__version__\s*=\s*['\"]([^'\"]+)['\"]$" \
 | sed -e "s|^__version__\s*=\s*['\"]||" \
 | sed -e "s|['\"]$||" \

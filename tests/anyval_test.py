import sys
from datetime import date
from typing import *
from unittest import TestCase, skipIf

from dataclasses import dataclass
from dataclasses_json import DataClassJsonMixin

from functional.anyval import AnyVal
from functional.final import FinalInheritanceError

try:
    date_fromisoformat = date.fromisoformat
except AttributeError:
    import re
    def date_fromisoformat(date_string: str) -> date:
        try:
            return date(**{ k: int(v) for k, v in re.fullmatch(r'(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})', date_string).groupdict().items() })
        except Exception as e:
            raise ValueError(f"Cannot parse {date.__qualname__!r} from {date_string!r}") from e

class AnyValTestCase(TestCase):
    
    MyIdClass: Type[AnyVal[str]]
    DateClass: Type[AnyVal[date]]
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        
        class MyID(AnyVal[int]): pass
        class Date(AnyVal[date]):
            @classmethod
            def decode_value(cls, data: str) -> date:
                if (not isinstance(data, str)):
                    raise TypeError(f"Cannot decode {date.__name__!r} from type {type(data).__qualname__!r}, ISO-format string required")
                
                return date_fromisoformat(data)
            
            def encode_value(self) -> str:
                return self.value.isoformat()
        
        cls.MyIdClass = MyID
        cls.DateClass = Date
    
    def test_constructor(self):
        self.assertEqual(self.MyIdClass(231).value, 231)
        self.assertEqual(self.DateClass(date_fromisoformat('2002-06-15')).value, date(2002, 6, 15))
    
    @skipIf(sys.version_info < (3, 7), reason="Coders are not supported for Python 3.6")
    def test_coder(self):
        MyID = self.MyIdClass
        Date = self.DateClass
        
        @dataclass
        class Person(DataClassJsonMixin):
            id: MyID
            name: str
            born: Date
        
        peter = Person(MyID(15), name='peter', born=Date(date(1990, 1, 12)))
        peter_json = '''{"id": 15, "name": "peter", "born": "1990-01-12"}'''
        self.assertEqual(peter.to_json(), peter_json)
        self.assertEqual(Person.from_json(peter_json), peter)
        
        mark = Person(MyID(-131239231231), name='mark', born=Date(date_fromisoformat('2002-06-15')))
        mark_json = '''{"id": -131239231231, "name": "mark", "born": "2002-06-15"}'''
        self.assertEqual(mark.to_json(), mark_json)
        self.assertEqual(Person.from_json(mark_json), mark)
    
    def test_repr(self):
        self.assertEqual(repr(self.MyIdClass(22)), 'MyID(22)')
        self.assertEqual(repr(self.DateClass(date_fromisoformat('2002-06-15'))), 'Date(datetime.date(2002, 6, 15))')
    
    def test_eq(self):
        id = self.MyIdClass(5)
        self.assertEqual(id, self.MyIdClass(5))
        self.assertNotEqual(id, self.MyIdClass(4))
        self.assertNotEqual(id, 5)
        
        class OtherID(AnyVal[int]): pass
        other = OtherID(5)
        self.assertNotEqual(id, other)
        self.assertEqual(id.value, other.value)
    
    @skipIf(sys.version_info < (3, 7), reason="Errors are not raised for generic final classes in Python 3.6")
    def test_no_inheritance(self):
        with self.assertRaises(FinalInheritanceError) as cm:
            class ExtendedID(self.MyIdClass):
                @property
                def full_id(self) -> str:
                    return repr(self.value)
            
            e = ExtendedID(15)
            _ = e.full_id
        
        self.assertEqual(cm.exception.cls, self.MyIdClass)


__all__ = \
[
    'AnyValTestCase',
]


if (__name__ == '__main__'):
    from unittest import main as unittests_main
    unittests_main()

import itertools
from typing import *
from unittest import TestCase

from functional.chaintools import *

T = TypeVar('T')
class ChainToolsTestCase(TestCase):
    @classmethod
    def coll2_to_list2(cls, it: Iterable[Iterable[T]]) -> List[List[T]]:
        return list(map(list, it))
    
    def assertIterableEqual(self, iter1: Iterable[T], iter2: Iterable[T]):
        self.assertListEqual(list(iter1), list(iter2))
    def assertIterable2Equal(self, iter1: Iterable[Iterable[T]], iter2: Iterable[Iterable[T]]):
        self.assertListEqual(self.coll2_to_list2(iter1), self.coll2_to_list2(iter2))
    
    # region Iterables operations
    def test_chunks(self):
        initial_items = list(range(100))
        cs = list(chunks(iter(initial_items), 17))
        tail = list(range(17 * 5, 100))
        self.assertTrue(all(len(c) == 17 or c == tail for c in cs), "Some elements have wrong length")
        self.assertIterableEqual(initial_items, itertools.chain.from_iterable(cs))
    
    def test_map_items(self):
        c = chunks(range(10), 3)
        actual = map_items(lambda x: x*x, c)
        expected = [[0, 1, 4], [9, 16, 25], [36, 49, 64], [81]]
        self.assertIterable2Equal(actual, expected)
    
    def test_apply(self):
        triggered = 0
        accumulated_items = list()
        def f(el):
            nonlocal triggered
            accumulated_items.append(el)
            triggered += 1
        
        cnt = 25
        initial_items = list(range(cnt))
        apply(f, iter(initial_items))
        
        actual = triggered
        expected = cnt
        self.assertEqual(actual, expected, f"Got unexpected numbers of function 'f' calls, expected: {expected}, actual: {actual}")
        self.assertListEqual(accumulated_items, initial_items)
    
    def test_apply_items(self):
        triggered = 0
        accumulated_items = list()
        def f(el):
            nonlocal triggered
            accumulated_items.append(el)
            triggered += 1
        
        cnt = 25
        initial_items = list(range(cnt))
        apply_items(f, chunks(initial_items, 4))
        
        actual = triggered
        expected = cnt
        self.assertEqual(actual, expected, f"Got unexpected numbers of function 'f' calls, expected: {expected}, actual: {actual}")
        self.assertListEqual(accumulated_items, initial_items)
    # endregion
    
    # region Chaining
    def test_chain(self):
        number = '1234'
        actual = chain(number, int, bin, len) - 2
        expected = len(bin(int(number))) - 2
        self.assertEqual(actual, expected)
    
    def test_chain_map(self):
        numbers = [ '123', '1234', '7', '18123' ]
        actual = chain_map(numbers, int, bin, len, lambda x: x - 2)
        expected = map(lambda el: len(bin(int(el))) - 2, numbers)
        self.assertIterableEqual(actual, expected)
    
    def test_chain_map_items(self):
        numbers = [ [ '1','2','3','4' ], [ '10', '21', '42', '63' ], [ '123', '234', '345', '999' ] ]
        actual = chain_map_items(numbers, int, bin, len, lambda x: x - 2)
        expected = ((len(bin(int(x))) - 2 for x in nums) for nums in numbers)
        self.assertIterable2Equal(actual, expected)
    # endregion
    
    # ToDo:
    #  - Lazy computations tests


__all__ = \
[
    'ChainToolsTestCase',
]


if (__name__ == '__main__'):
    from unittest import main as unittests_main
    unittests_main()

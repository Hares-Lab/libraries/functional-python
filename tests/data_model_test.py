import builtins
from inspect import isclass
from types import ModuleType

import functional
import functional.builtins_wrapper
from tests.utils.inspector import *

class DataModelTestCase(PackageInspectorTestCase):
    root = functional
    
    def assertHasAttr(self, class_or_instance, attr: str, msg: str = None):
        if (not hasattr(class_or_instance, attr)):
            prefix = 'class' if isclass(class_or_instance) else 'instance'
            standardMsg = f"{prefix.title()} {class_or_instance!r} does not have the required attribute {attr!r}"
            self.fail(self._formatMessage(msg, standardMsg))
    
    def assertNotHasAttr(self, class_or_instance, attr: str, msg: str = None):
        if (hasattr(class_or_instance, attr)):
            prefix = 'class' if isclass(class_or_instance) else 'instance'
            standardMsg = f"{prefix.title()} {class_or_instance!r} has attribute {attr!r}, but it should not"
            self.fail(self._formatMessage(msg, standardMsg))
    
    @discover_items(ItemFilters.AnyClass, ItemFilters.Inversed | ItemFilters.ExceptionClass, ItemFilters.Inversed | ItemFilters.EnumClass, match_mode=MatchMode.All, recursive=True, undeclared_ok=True, non_package_ok=False)
    def test_has_slots(self, module: ModuleType, key: str, cls: type):
        """ Check all classes (non-Enums, non-Exceptions) have __slots__ field """
        self.assertHasAttr(cls, '__slots__')
        
        # ToDo:
        #  - Check all classes' *instances* do not have __dict__
    
    @discover_items(ItemFilters.AnyClass, ItemFilters.Inversed | ItemFilters.ExceptionClass, match_mode=MatchMode.All, recursive=True, undeclared_ok=True)
    def test_no_dataclasses(self, module: ModuleType, key: str, cls: type):
        """ Check no classes (non-Exceptions) are actual dataclasses """
        if (issubclass(cls, functional.AbstractFilter)):
            self.skipTest(f"{functional.AbstractFilter.__name__!r} and its children are allowed to be dataclasses.")
        self.assertIsNotDataClass(cls)
    
    def test_builtins_wrapper(self):
        # noinspection PyTypeChecker
        with self.newRoot(functional.builtins_wrapper):
            found = { key: item for _, key, item in self.detect_items(ItemFilters.Inversed | ItemFilters.Module, undeclared_ok=False, recursive=False) }
            expected = dict(map=builtins.map, sum=builtins.sum)
            self.assertDictEqual(found, expected)


__all__ = \
[
    'DataModelTestCase',
]


if (__name__ == '__main__'):
    from unittest import main as unittests_main
    unittests_main()

import random
# noinspection PyProtectedMember
from abc import ABC
from collections import OrderedDict
from dataclasses import dataclass, _PARAMS, _DataclassParams
from functools import lru_cache
from hashlib import sha256
from typing import *
from unittest import TestCase, main

from functional.chaintools import chain
from functional.filters import *
from tests.utils.stdout_tests import OutputContextManager

class AbstractFilterTestCase(TestCase):
    def test_frozen_state(self):
        with self.subTest("Check dataclass attribute"):
            _frozen = 'frozen'
            params: _DataclassParams = getattr(AbstractFilter, _PARAMS)
            self.assertTrue(params.frozen, f"Dataclass parameter {_frozen!r} of class {AbstractFilter.__name__!r} should be exactly 'True'")
        
        # Unstable and thus disabled
        for Parent, subscripted in [ (AbstractFilter, False), (AbstractFilter[int], True) ]: # type: Type[AbstractFilter], bool
            with self.subTest("Check raising exception on nesting", subscripted=False):
                self.skipTest("This test is unstable")
                with self.assertRaises(TypeError):
                    # noinspection PyDataclass, PyUnusedLocal
                    @dataclass
                    class NewFilter(Parent):
                        def check_element(self, el: int) -> bool:
                            return el > 0
    
    def test_abstractmethod_raises(self):
        with self.assertRaises(TypeError):
            # noinspection PyAbstractClass
            @dataclass(frozen=True)
            class NewFilter(AbstractFilter[int]):
                param: int
            _ = NewFilter(6)
    
    def test_inheritance_ok(self):
        for Parent, subscripted in [ (AbstractFilter, False), (AbstractFilter[int], True) ]: # type: Type[AbstractFilter], bool
            with self.subTest(subscripted=False):
                @dataclass(frozen=True)
                class NewFilter(Parent):
                    param: int = 0
                    def check_element(self, el) -> bool:
                        return el > self.param
                
                _ = NewFilter(6)
    
    def test_is_callable(self):
        @dataclass(frozen=True)
        class NewFilter(AbstractFilter[int]):
            def check_element(self, el: int) -> bool:
                return el > 0
        self.assertTrue(callable(NewFilter()))
    
    def test_hashing(self):
        @lru_cache()
        def filtered_range(start: int, end: int, filter: Callable[[int], bool]) -> List[int]:
            return [ el for el in range(start, end) if (filter(el)) ]
        
        @dataclass(frozen=True)
        class DigitSumFilter(AbstractFilter[int]):
            mod: int
            
            def check_element(self, el: int) -> bool:
                if (el < 0): el = -el
                digits_sum = sum(map(int, chain(el, str, list)))
                return digits_sum % self.mod == 0
        
        filtered_range(6, 120, DigitSumFilter(3))
        filtered_range(7, 20, DigitSumFilter(4))
        filtered_range(6, 120, DigitSumFilter(4))
        filtered_range(6, 120, DigitSumFilter(3))
        filtered_range(7, 20, DigitSumFilter(4))
        
        cache_info = filtered_range.cache_info()
        self.assertEqual(cache_info.hits, 2)
        self.assertEqual(cache_info.misses, 3)
        self.assertEqual(cache_info.currsize, 3)


class FilterExampleTestCase(TestCase):
    def test_filter_ways(self):
        max_len = 10000
        param1 = IsNotNoneFilter
        param2 = object()
        @dataclass(frozen=True)
        class MyClass():
            x: float
            s: str
        seq = [ MyClass(random.random(), f'any str x{random.randint(0, max_len)}') for _ in range(max_len) ]
        
        def super_hash(x: Any) -> int:
            return int(sha256(str(hash(x)).encode()).hexdigest(), base=16)
        def my_func(el: Any, o1: Any, o2: Any):
            return super_hash(o1) < super_hash(el) < super_hash(o2) or super_hash(o1) > super_hash(el) > super_hash(o2)
        
        @dataclass(frozen=True)
        class MyFilter(AbstractFilter[Any]):
            o1: object
            o2: object
            def check_element(self, el: Any) -> bool:
                return my_func(el, self.o1, self.o2)
        
        with self.subTest("filter built-in + function"):
            expected = list(filter(lambda el: my_func(el, param1, param2), seq))
            self.assertGreater(len(expected), 0)
            self.assertLess(len(expected), max_len)
        
        with self.subTest("filter class instance method"):
            filtered = list(MyFilter(param1, param2).filter(seq))
            self.assertSequenceEqual(filtered, expected)
        
        with self.subTest("filter built-in + class instance __call__"):
            filtered = list(filter(MyFilter(param1, param2), seq))
            self.assertSequenceEqual(filtered, expected)
    
    def test_GEFilter(self):
        with OutputContextManager() as cm:
            @dataclass(frozen=True)
            class GEFilter(AbstractFilter[int]):
                than: int

                def check_element(self, el: int) -> bool:
                    return el >= self.than
            
            lst = [ -1, 3, 8, 5, 0, -6, 7 ]
            for el in GEFilter(5).filter(lst):
                print(el)
            # Output:
            # 8 5 7
        
        expected = \
        """
        8
        5
        7
        """
        expected = '\n'.join(map(str.lstrip, expected.lstrip().splitlines()))
        self.assertMultiLineEqual(expected, cm.stdout.getvalue())


class HasAttrFilterTestCase(TestCase):
    def test_checking(self):
        filter = HasAttrFilter('__getitem__')
        options = \
        [
            (dict(), True),
            (0, False),
            (list(), True),
            ('', True),
            (OrderedDict, True),
            (object(), False),
            (type(None), False),
        ]
        for arg, expected in options:
            with self.subTest(arg=arg, type=type(arg).__name__):
                self.assertEqual(filter.check_element(arg), expected)


class IsNotNoneFilterTestCase(TestCase):
    def test_checking(self):
        filter = IsNotNoneFilter
        options = \
        [
            (None, False),
            (True, True),
            (False, True),
            (0, True),
            ('', True),
            (object(), True),
            (type(None), True),
        ]
        for arg, expected in options:
            with self.subTest(arg=arg, type=type(arg).__name__):
                self.assertEqual(filter.check_element(arg), expected)

class LogicalFiltersTestCase(TestCase, ABC):
    @dataclass(frozen=True)
    class _OKFilter(AbstractFilter[Any]):
        def check_element(self, el: Any) -> bool:
            return True
    @dataclass(frozen=True)
    class _NoFilter(AbstractFilter[Any]):
        def check_element(self, el: Any) -> bool:
            return False
    
    OKFilter = _OKFilter()
    NoFilter = _NoFilter()
    
    def check_test_all(self, *data_set: Tuple[AbstractFilter, bool]):
        for filter, expected in data_set:
            with self.subTest(f"{filter} => {expected}"):
                self.assertEqual(filter.check_element(Any), expected)

class AndFilterTestCase(LogicalFiltersTestCase):
    def test_single_element(self):
        self.check_test_all \
        (
            (AndFilter([ self.OKFilter ]), True),
            (AndFilter([ self.NoFilter ]), False),
        )
    
    def test_empty_collection(self):
        self.check_test_all \
        (
            (AndFilter([ ]), True),
        )
    
    def test_multiple_filters(self):
        self.check_test_all \
        (
            (AndFilter([ self.OKFilter, self.OKFilter, self.OKFilter ]), True),
            (AndFilter([ self.NoFilter, self.OKFilter, self.OKFilter ]), False),
            (AndFilter([ self.OKFilter, self.NoFilter, self.OKFilter ]), False),
            (AndFilter([ self.OKFilter, self.OKFilter, self.NoFilter ]), False),
            (AndFilter([ self.NoFilter, self.NoFilter, self.OKFilter ]), False),
            (AndFilter([ self.NoFilter, self.OKFilter, self.NoFilter ]), False),
            (AndFilter([ self.OKFilter, self.NoFilter, self.NoFilter ]), False),
            (AndFilter([ self.NoFilter, self.NoFilter, self.NoFilter ]), False),
        )

class OrFilterTestCase(LogicalFiltersTestCase):
    def test_single_element(self):
        self.check_test_all \
        (
            (OrFilter([ self.OKFilter ]), True),
            (OrFilter([ self.NoFilter ]), False),
        )
    
    def test_empty_collection(self):
        self.check_test_all \
        (
            (OrFilter([ ]), False),
        )
    
    def test_multiple_filters(self):
        self.check_test_all \
        (
            (OrFilter([ self.OKFilter, self.OKFilter, self.OKFilter ]), True),
            (OrFilter([ self.NoFilter, self.OKFilter, self.OKFilter ]), True),
            (OrFilter([ self.OKFilter, self.NoFilter, self.OKFilter ]), True),
            (OrFilter([ self.OKFilter, self.OKFilter, self.NoFilter ]), True),
            (OrFilter([ self.NoFilter, self.NoFilter, self.OKFilter ]), True),
            (OrFilter([ self.NoFilter, self.OKFilter, self.NoFilter ]), True),
            (OrFilter([ self.OKFilter, self.NoFilter, self.NoFilter ]), True),
            (OrFilter([ self.NoFilter, self.NoFilter, self.NoFilter ]), False),
        )

class NotFilterTestCase(LogicalFiltersTestCase):
    def test_single_element(self):
        self.check_test_all \
        (
            (NotFilter(self.OKFilter), False),
            (NotFilter(self.NoFilter), True),
        )


__all__ = \
[
    'AbstractFilterTestCase',
    'AndFilterTestCase',
    'FilterExampleTestCase',
    'HasAttrFilterTestCase',
    'IsNotNoneFilterTestCase',
    'LogicalFiltersTestCase',
    'NotFilterTestCase',
    'OrFilterTestCase',
]


if (__name__ == '__main__'):
    exit_code = main()
    exit(exit_code)

import sys
from typing import *
from unittest import TestCase, skipIf

from functional.final import *
from functional.typing_wrapper import *

T = TypeVar('T')
class FinalClassTestCase(TestCase):
    
    ParentClass: Type
    ChildClass: Type
    GenericParentClass: Type
    GenericChildClass: Type
    
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        
        class SomeClass:
            some_field: str = 'test'
            def __init_subclass__(cls, **kwargs):
                super().__init_subclass__(**kwargs)
                cls.ninja_field = 'shuriken'
        
        @final
        @final_class
        class SomeOtherClass(SomeClass):
            def __init__(self, param: str):
                super().__init__()
                self.some_field = param
        
        cls.ParentClass = SomeClass
        cls.ChildClass = SomeOtherClass
        
        class SomeGenericClass(Generic[T]):
            some_field: T
            def __init_subclass__(cls, **kwargs):
                super().__init_subclass__(**kwargs)
                cls.ninja_field = 'shuriken'
        
        @final
        @final_class
        class SomeOtherGenericClass(SomeGenericClass[T], Generic[T]):
            def __init__(self, param: T):
                super().__init__()
                self.some_field = param
        
        cls.GenericParentClass = SomeGenericClass
        cls.GenericChildClass = SomeOtherGenericClass
    
    def test_parent_init_subclass(self):
        self.assertNotIn('ninja_field', self.ParentClass.__dict__)
        self.assertIn('ninja_field', self.ChildClass.__dict__)
        # noinspection PyUnresolvedReferences
        self.assertEqual(self.ChildClass.ninja_field, 'shuriken')
        instance = self.ChildClass('data')
        self.assertEqual(instance.some_field, 'data')
        self.assertEqual(instance.ninja_field, 'shuriken')
    
    def test_no_inheritance(self):
        with self.assertRaises(FinalInheritanceError) as cm:
            class ExtendedOtherClass(self.ChildClass):
                @property
                def full_id(self) -> str:
                    # noinspection PyUnresolvedReferences
                    return repr(self.some_field)

            # noinspection PyArgumentList
            e = ExtendedOtherClass('1122')
            _ = e.full_id
        
        self.assertEqual(cm.exception.cls, self.ChildClass)
        self.assertEqual(cm.exception.message, f"Cannot inherit from final class {repr(self.ChildClass.__qualname__)}.")
    
    @skipIf(sys.version_info < (3, 7), reason="Generics just don't work properly on Python 3.6")
    def test_generics_init_subclass(self):
        self.assertNotIn('ninja_field', self.GenericParentClass.__dict__)
        self.assertIn('ninja_field', self.GenericChildClass.__dict__)
        # noinspection PyUnresolvedReferences
        self.assertEqual(self.GenericChildClass.ninja_field, 'shuriken')
        instance = self.GenericChildClass('data')
        self.assertEqual(instance.some_field, 'data')
        self.assertEqual(instance.ninja_field, 'shuriken')
    
    # noinspection PyUnresolvedReferences
    def test_generics_instantiate(self):
        ParentClass = self.GenericParentClass
        ChildClass = self.GenericChildClass
        
        with self.subTest("Check no instantiate"):
            parent: ParentClass[int] = self.GenericParentClass()
            child: ChildClass[int] = self.GenericChildClass(11)
        
        with self.subTest("Check with instantiate"):
            parent: ParentClass[str] = self.GenericParentClass[str]()
            child: ChildClass[str] = self.GenericChildClass[str]('text')
    
    def test_generic_no_inheritance(self):
        if (sys.version_info < (3, 7)):
            context_mgr_gen = self.assertWarns(FinalInheritanceWarning)
            context_mgr_attr = 'warning'
            error_msg = f"Class {self.GenericChildClass.__qualname__!r}: Attempting to inherit from generic final class. However, on Python 3.6.x this could not be handled"
        
        else:
            context_mgr_gen = self.assertRaises(FinalInheritanceError)
            context_mgr_attr = 'exception'
            error_msg = f"Cannot inherit from final class {repr(self.GenericChildClass.__qualname__)}."
        
        with context_mgr_gen as cm:
            class ExtendedOtherClass(self.GenericChildClass):
                @property
                def full_id(self) -> str:
                    # noinspection PyUnresolvedReferences
                    return repr(self.some_field)
            
            # noinspection PyArgumentList
            e = ExtendedOtherClass('1122')
            _ = e.full_id
        
        error_or_warning = getattr(cm, context_mgr_attr)
        self.assertEqual(error_or_warning.cls, self.GenericChildClass)
        self.assertEqual(error_or_warning.message, error_msg)
        
        with context_mgr_gen as cm:
            # noinspection PyUnresolvedReferences
            class ExtendedOtherClass(self.GenericChildClass[str]):
                @property
                def full_id(self) -> str:
                    # noinspection PyUnresolvedReferences
                    return repr(self.some_field)

            # noinspection PyArgumentList
            e = ExtendedOtherClass('1122')
            _ = e.full_id
        
        error_or_warning = getattr(cm, context_mgr_attr)
        self.assertEqual(error_or_warning.cls, self.GenericChildClass)
        self.assertEqual(error_or_warning.message, error_msg)


__all__ = \
[
    'FinalClassTestCase',
]


if (__name__ == '__main__'):
    from unittest import main as unittests_main
    unittests_main()

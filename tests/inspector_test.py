from functools import reduce
from types import ModuleType, FunctionType
from typing import *

import tests.sample_package
import tests.sample_package.members as M
from functional.option import *
from .sample_package.classes import *
from .utils import dict_sum
from .utils.inspector import *
from .utils.inspector import ItemFilters

T = TypeVar('T')
def sort_by_name(iterable: Iterable[T]) -> Iterator[T]:
    return sorted(iterable, key=lambda cls: cls.__name__)

class InspectorToolTestCase(PackageInspectorTestCase):
    root = tests.sample_package
    
    # region Member Type Checks
    @discover_items(ItemFilters.Function, recursive=True, undeclared_ok=True)
    def test_find_functions(self, module: ModuleType, key: str, func: FunctionType):
        """ Check any functions could be found, and all of them are functions """
        print(f'{module.__name__}.{key}: {func!r}')
        self.assertIsFunction(func)
        self.assertIsNotModule(func)
        self.assertIsNotClass(func)
    
    @discover_items(ItemFilters.Module, recursive=True, undeclared_ok=True)
    def test_find_modules(self, module: ModuleType, key: str, mod: ModuleType):
        """ Check any modules could be found, and all of them are modules """
        print(f'{module.__name__}.{key}: {mod!r}')
        self.assertIsNotFunction(mod)
        self.assertIsModule(mod)
        self.assertIsNotClass(mod)
    
    @discover_items(ItemFilters.AnyClass, recursive=True, undeclared_ok=True)
    def test_find_classes(self, module: ModuleType, key: str, cls: type):
        """ Check any classes could be found, and all of them are classes """
        print(f'{module.__name__}.{key}: {cls!r}')
        self.assertIsNotFunction(cls)
        self.assertIsNotModule(cls)
        self.assertIsClass(cls)
    
    @discover_items(ItemFilters.Property, recursive=True, undeclared_ok=True)
    def test_find_properties(self, module: ModuleType, key: str, prop: Any):
        """ Check any properties could be found, and all of them are properties """
        print(f'{module.__name__}.{key}: {repr(prop)[:1000]}')
        self.assertIsNotFunction(prop)
        self.assertIsNotModule(prop)
        self.assertIsNotClass(prop)
    # endregion
    
    def discover_and_assert(self, sub_tests: Dict[ItemFilters, Dict[str, Option[T]]], *, new_root: Option[ModuleType] = Option.empty, **discovery_params):
        """
        Discover each of filter in `sub_tests` keys using the `PackageInspector.detect_items` method.
        For each key, check that entries found are equal to the expected.
        
        Args:
            sub_tests: `Dict[ItemFilters, Dict[str, Option[T]]]`.
                A mapping between `ItemFilters` for `PackageInspector.detect_items` method and the expected result.
                Expected result is a mapping of partial key (only right part) and an `Option`'al item.
                Value checks are skipped for empty options.
            
            new_root: `Option[types.ModuleType] = Option.empty`.
                An optional parameter for the temporary new package root.
            
            **discovery_params:
                Extra parameters that are passed as `**kwargs` to `PackageInspector.detect_items`.
        """
        
        if (new_root.non_empty):
            with self.newRoot(new_root.get):
                return self.discover_and_assert(sub_tests, **discovery_params, new_root=Option.empty)
        
        for filter, expected in sub_tests.items():
            with self.subTest(class_type=filter.name):
                found = list(self.detect_items(filter, **discovery_params))
                
                print(f"Found {len(found)} {filter.name} items:")
                for module, key, _ in found:
                    print(f' * {module.__name__}.{key}')
                print()
                
                actual = { key: expected.get(key, Option.empty) and Some(item) for _, key, item in found }
                self.assertDictEqual(actual, expected)
    
    def test_all_classes(self):
        """ Find all of the classes groups """
        
        _sub_tests: Dict[ItemFilters, List[type]] = \
        {
            ItemFilters.RegularClass:   [ AbstractClassImpl, RegularClass ],
            ItemFilters.AbstractClass:  [ AbstractClass ],
            ItemFilters.ExceptionClass: [ ExceptionClass, ExceptionDataClass ],
            ItemFilters.DataClass:      [ DataClass, ExceptionDataClass ],
            ItemFilters.EnumClass:      [ EnumClass, EnumFlagClass ],
        }
        
        sub_tests: Dict[ItemFilters, Dict[str, Option[type]]] = { k: { cls.__name__: Some(cls) for cls in v } for k, v in _sub_tests.items() }
        sub_tests[ItemFilters.EnumClass]['RedeclaredClass'] = Some(RedeclaredClass)
        sub_tests[ItemFilters.AnyClass] = reduce(dict_sum, sub_tests.values(), dict())
        self.discover_and_assert(sub_tests, new_root=Some(tests.sample_package.classes), recursive=False, undeclared_ok=False)
    
    def test_visibility(self):
        """ Find all members, including undeclared, _protected, and __private """
        
        sub_tests: Dict[ItemFilters, Dict[str, Option[Any]]] = \
        {
            ItemFilters.Visible:
            {
                'tests.sample_package.members': Option.empty,
                'UndeclaredClass': Some(M.UndeclaredClass),
                'declared_bound_class_method': Some(M.declared_bound_class_method),
                'declared_bound_method': Some(M.declared_bound_method),
                'declared_func': Some(M.declared_func),
                'declared_member': Some(M.declared_member),
                'redeclared_function': Some(M.redeclared_function),
                'undeclared_function': Some(M.undeclared_function),
                'undeclared_member': Some(M.undeclared_member),
            },
            
            ItemFilters.Protected:
            {
                '_ProtectedUndeclaredClass': Some(M._ProtectedUndeclaredClass),
                '_protected_function': Some(M._protected_function),
                '_protected_member': Some(M._protected_member),
            },
            
            ItemFilters.Private:
            {
                '__private_func': Some(getattr(M, '__private_func')),
                '__private_member': Some(getattr(M, '__private_member')),
            },
            
            ItemFilters.Reserved:
            {
                '__all__': Some(M.__all__),
                '__builtins__': Option.empty,
                '__cached__': Option.empty,
                '__doc__': Some(M.__doc__),
                '__file__': Some(M.__file__),
                '__name__': Some(M.__name__),
                '__package__': Option.empty,
                '__reserved_func__': Some(M.__reserved_func__),
                '__reserved_member__': Some(M.__reserved_member__),
            },
        }
        
        self.discover_and_assert(sub_tests, new_root=Some(tests.sample_package.members), recursive=False, undeclared_ok=True)


__all__ = \
[
    'InspectorToolTestCase',
]


if (__name__ == '__main__'):
    from unittest import main as unittests_main
    unittests_main()

import sys
from typing import *
from unittest import TestCase

from dataclasses import dataclass

from functional.option import *
from tests.utils.decorations import make_for_decorator

@dataclass
class TestClass:
    a: int

def for_options(test_method: Callable[['OptionsTestCase', List[Option], bool], None]) -> Callable[['OptionsTestCase'], None]:
    return make_for_decorator(lambda self: self.get_options(), lambda _, is_defined: dict(is_defined=is_defined))(test_method)

def for_each_option(test_method: Callable[['OptionsTestCase', Option, bool], None]) -> Callable[['OptionsTestCase'], None]:
    return make_for_decorator(lambda self: ((o, is_defined) for options, is_defined in self.get_options() for o in options), lambda o, *_: dict(option=o))(test_method)

_MISSING = object()

class OptionsTestCase(TestCase):
    a = Some(5)
    b = none
    _a = ('a', a)
    _b = ('b', b)
    
    lst: List[Tuple[str, Option[int]]] = [ _a, _b ]
    
    @classmethod
    def get_options(cls) -> List[Tuple[List[Option], bool]]:
        options = \
        [
            ([ none, Option(None) ], False),
            ([ Some(None), Some(4), Option(4), Some(TestClass(2)), Option(TestClass(2)), Some('test'), Option('test') ], True),
        ]
        return options
    
    def test_to_str(self):
        with self.subTest("Empty option"):
            self.assertEqual(str(none), "None")
        with self.subTest("Non-empty option #1"):
            self.assertEqual(str(Some(4)), "Some(4)")
        with self.subTest("Non-empty option #2"):
            self.assertEqual(str(Some(None)), "Some(None)")
        with self.subTest("Non-empty option #3"):
            self.assertEqual(str(Some('test')), "Some('test')")
        with self.subTest("Non-empty option #4"):
            self.assertEqual(str(Some(TestClass(8))), "Some(TestClass(a=8))")
    
    def test_get(self):
        with self.subTest("Empty option"):
            self.assertRaises(EmptyOption, lambda: none.get)
        for i, param in enumerate([ 4, None, 'test', TestClass(8) ]):
            o = Some(param)
            with self.subTest(f"Non-empty option #{i}", option=o):
                self.assertEqual(o.get, param)
    
    def test_flatten_1(self):
        with self.subTest("Empty option"):
            self.assertEqual(none.flatten, none)
        with self.subTest("Non-empty option #1"):
            self.assertEqual(Some(4).flatten, 4)
        with self.subTest("Non-empty option #2"):
            self.assertEqual(Some(None).flatten, None)
        with self.subTest("Non-empty option #3"):
            self.assertEqual(Some('test').flatten, 'test')
        with self.subTest("Non-empty option #4"):
            self.assertEqual(Some(TestClass(8)).flatten, TestClass(8))
        with self.subTest("Some(None)"):
            self.assertEqual(Some(none).flatten, none)
    
    @for_each_option
    def test_flatten_2(self, o: Option, is_defined: bool):
        self.assertEqual(Some(o).flatten, o)
        self.assertEqual(Some(Some(o)).flatten, Some(o))
    
    @for_each_option
    def test_empty(self, o: Option, is_defined: bool):
        self.assertEqual(o.is_empty, not is_defined)
        self.assertEqual(o.non_empty, is_defined)
        self.assertEqual(o.is_defined, is_defined)
        self.assertEqual(bool(o), is_defined)
    
    @for_each_option
    def test_foreach(self, o: Option, is_defined: bool):
        x = _MISSING
        def set_x(val):
            nonlocal x
            x = val
        
        o.foreach(set_x)
        if (is_defined):
            self.assertEqual(x, o.get)
        else:
            self.assertIs(x, _MISSING)
        
        del x, set_x
    
    @for_each_option
    def test_map(self, o: Option, is_defined: bool):
        result = o.map(str)
        if (is_defined):
            self.assertEqual(result, Some(str(o.get)))
        else:
            self.assertEqual(result, none)
        
        del result
    
    @for_each_option
    def test_flat_map(self, o: Option, is_defined: bool):
        with self.subTest(map_return='None'):
            self.assertEqual(o.flat_map(lambda _: none), none)
        with self.subTest(map_return='Some'):
            result = o.flat_map(lambda _: Some(str(_)))
            if (is_defined):
                self.assertEqual(result, Some(str(o.get)))
            else:
                self.assertEqual(result, none)
            
            del result
    
    @for_each_option
    def test_get_or_else(self, o: Option, is_defined: bool):
        result = o.get_or_else("temp")
        if (is_defined):
            self.assertEqual(result, o.get)
        else:
            self.assertEqual(result, "temp")
        
        del result
    
    @for_each_option
    def test_lazy_or(self, o: Option, is_defined: bool):
        triggered = False
        def get_else():
            nonlocal triggered
            triggered = True
            return Some("temp")
        
        result = (o or get_else()).get
        if (is_defined):
            self.assertEqual(result, o.get)
            self.assertFalse(triggered)
        else:
            self.assertEqual(result, "temp")
            self.assertTrue(triggered)
        
        del triggered, result
    
    @for_each_option
    def test_as_optional(self, o: Option, is_defined: bool):
        result = o.as_optional
        if (is_defined):
            self.assertEqual(result, o.get)
        else:
            self.assertIsNone(result)
        
        if (result is not None or not is_defined):
            self.assertEqual(o, Option(o.as_optional))
    
    @for_each_option
    def test_is_option__true(self, o: Option, is_defined: bool):
        """ Positive-case tests for `Option.is_option()` """
        self.assertTrue(Option.is_option(o))
        self.assertTrue(is_option(o))
    
    @for_each_option
    def test_is_option__false(self, o: Option, is_defined: bool):
        """ Negative-case tests for `Option.is_option()`. Only for non-empty options """
        
        if (not is_defined):
            print(f"Skipping non-defined option {o}", file=sys.stderr)
            return
        
        v = o.get
        self.assertFalse(Option.is_option(v))
        self.assertFalse(is_option(v))
    
    @for_options
    def test_tuple_transform(self, options: List[Option], is_defined: bool):
        if (not is_defined):
            source = OptionNone
        else:
            source = Some(tuple(map(lambda o: o.get, options)))
        
        result = source.tuple_transform(len(options))
        expected = tuple(options)
        self.assertEqual(result, expected)
    
    def test_empty_some_warning(self):
        with self.assertWarns(EmptySomeWarning) as ctm:
            _ = Some(None)
        self.assertEqual(ctm.warning.message, "'Some' Option with 'None' value detected. Usually this is an error. "
                                              "If it is not, you can suppress this warning by setting `functional.option.DISABLE_WARNING = True`.")
    
    def test_empty_option_error(self):
        with self.assertRaises(EmptyOption) as ctm:
            _ = Option.empty.get
        self.assertEqual(ctm.exception.message, "Could not 'get' from an empty Option")


__all__ = \
[
    'OptionsTestCase',
]


if (__name__ == '__main__'):
    from unittest import main as unittests_main
    unittests_main()

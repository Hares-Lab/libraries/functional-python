from abc import ABC
from dataclasses import dataclass
from enum import Enum, auto, Flag
from typing import *

@dataclass
class DataClass:
    x: int
    y: int

class ExceptionClass(Exception):
    pass

@dataclass(frozen=True)
class ExceptionDataClass(Exception):
    cls: type
    
    def __post_init__(self):
        super().__init__(self.message)
    
    @property
    def message(self):
        return f"{self.cls} is not valid class"


class AbstractClass(ABC):
    value: Optional[str]

class AbstractClassImpl(AbstractClass):
    @property
    def value(self):
        return "test"

class EnumClass(Enum):
    EnumValue = auto()

class EnumFlagClass(Flag):
    FlagOne = auto()
    FlagTwo = auto()

class RegularClass:
    data: str

RedeclaredClass = Enum

__all__ = \
[
    'AbstractClass',
    'AbstractClassImpl',
    'DataClass',
    'EnumClass',
    'EnumFlagClass',
    'ExceptionClass',
    'ExceptionDataClass',
    'RedeclaredClass',
    'RegularClass',
]

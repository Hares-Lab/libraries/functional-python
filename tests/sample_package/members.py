import math
from functools import reduce

class UndeclaredClass:
    pass

class _ProtectedUndeclaredClass:
    def __init__(self, value):
        self.value = value
    def some_meth(self) -> str:
        return repr(self.value)
    @classmethod
    def some_cls_meth(cls) -> str:
        return cls.__name__

declared_member = _ProtectedUndeclaredClass(146)
declared_bound_method = _ProtectedUndeclaredClass.some_meth
declared_bound_class_method = _ProtectedUndeclaredClass.some_cls_meth

def declared_func():
    return 4

undeclared_member = 'easter egg'
def undeclared_function(arg: str):
    if (arg == 'easter egg'):
        print("This is an actual easter egg")

_protected_member = math.pi
def _protected_function():
    return hex(182)

redeclared_function = reduce

__private_member = 'pls dont touch me'
__reserved_member__ = 'are you sure?'

def __private_func():
    raise NotImplementedError
def __reserved_func__():
    print(repr(__reserved_member__))


__all__ = \
[
    'declared_bound_class_method',
    'declared_bound_method',
    'declared_func',
    'declared_member',
    'redeclared_function',
]

from copy import copy
from typing import *

K = TypeVar('K')
V = TypeVar('V')
def dict_sum(d1: Dict[K, V], d2: Dict[K, V]) -> Dict[K, V]:
    d1 = copy(d1)
    d1.update(d2)
    return d1

__all__ = \
[
    'dict_sum',
]

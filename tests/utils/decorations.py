from functools import wraps
from typing import *
from unittest import TestCase

T = TypeVar('T')
def cast_to_tuple(arg: Union[Tuple[T, ...], T]) -> Tuple[T, ...]:
    if (isinstance(arg, tuple)):
        return arg
    else:
        return (arg, )

TC = TypeVar('TC', bound=TestCase)
def make_for_decorator(iter_func: Callable[[TC], Iterator[Any]], subtest_keys_func: Callable[[Any], Dict[str, Any]]):
    def decorator(func) -> Callable[[TC], None]:
        @wraps(func)
        def wrapper(self: TC):
            self: TestCase
            anything_found: bool = False
            for arg in iter_func(self):
                args = cast_to_tuple(arg)
                anything_found = True
                with self.subTest(**subtest_keys_func(*args)):
                    func(self, *args)
            else:
                self.assertTrue(anything_found, "No items were discovered to test")
        
        return wrapper
    return decorator


__all__ = \
[
    'cast_to_tuple',
    'make_for_decorator',
]

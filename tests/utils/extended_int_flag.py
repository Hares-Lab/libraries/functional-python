import itertools
import operator
from functools import reduce
from typing import *


def bits_gen() -> Iterator[int]:
    return map(lambda x: 2**x, itertools.count())

T = TypeVar('T')
def shift(it: Iterator[T], count: int = 1) -> Iterator[T]:
    for i in range(count):
        next(it)
    return it

def count_bits(x: int) -> int:
    return bin(x).count('1')

def shift_for_group(bits_it: Iterator[int], group_bits: int) -> Iterator[int]:
    return shift(bits_it, (count_bits(int(group_bits)) + 1) // 2)

class BitsCounter(Iterator[int]):
    __slots__ = ('_pos')
    _pos: int
    
    def __init__(self, position: int = 0):
        super().__init__()
        self._pos = position
    
    def __iter__(self):
        return self
    
    def __next__(self) -> int:
        value = self.upcoming
        self._pos += 1
        return value
    
    def __repr__(self):
        return f'{self.__class__.__name__}(position: {self._pos}, next: {self.upcoming})'
    
    def shift(self, count: int = 1):
        self._pos += count
        return self._pos
    
    def reset(self):
        self._pos = 0
    
    @property
    def upcoming(self) -> int:
        return 2 ** self._pos
    
    def next(self):
        return next(self)

class FlagGroup(ContextManager, Iterator[int]):
    __slots__ = ('parent_iterator', 'local_iterator', 'start', '_last_value', '_iter_gen')
    
    parent_iterator: Iterator[int]
    local_iterator: Optional[Iterator[int]]
    start: Optional[int]
    _last_value: int
    _iter_gen: Callable[[], Iterator[int]]
    
    def __init__(self, it: Iterator[int], iter_gen: Optional[Callable[[], Iterator[int]]] = None):
        self.parent_iterator = it
        self._iter_gen = iter_gen if (iter_gen is not None) else lambda: itertools.count(1)
        self.__cleanup__()
    
    def __cleanup__(self):
        self.local_iterator = None
        self.start = None
        self._last_value = 0
    
    def __enter__(self) -> Iterator[int]:
        self.start = next(self.parent_iterator)
        self.local_iterator = self._iter_gen()
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        shift(self.parent_iterator, count_bits(self.bit_mask) - 1)
        self.__cleanup__()
        return
    
    def __iter__(self) -> Iterator[int]:
        return self
    def __next__(self) -> int:
        self._last_value = next(self.local_iterator)
        return self.start * self._last_value
    
    def _iter_values(self) -> Iterator[int]:
        if (not self._last_value): return
        for x in self._iter_gen():
            yield x
            if (x >= self._last_value): break
    
    @property
    def values(self) -> List[int]:
        return [ self.start * x for x in self._iter_values() ]
    
    @property
    def bit_mask(self) -> int:
        return reduce(operator.__or__, self.values, 0)

def cast_to_tuple(arg: Union[Tuple[T, ...], T]) -> Tuple[T, ...]:
    if (isinstance(arg, tuple)):
        return arg
    else:
        return (arg, )

__all__ = \
[
    'bits_gen',
    'cast_to_tuple',
    'count_bits',
    'shift_for_group',
    
    'BitsCounter',
    'FlagGroup',
]

from enum import IntFlag, Enum
from typing import *

from tests.utils.extended_int_flag import FlagGroup, BitsCounter

class ItemFilters(IntFlag):
    __bits__: Iterator[int] = BitsCounter()
    _Empty = 0
    
    # Mutually Exclusive
    with FlagGroup(__bits__) as __group__: # type: FlagGroup
        Visible   = next(__group__)
        Protected = next(__group__)
        Private   = next(__group__)
        Reserved  = next(__group__)
        _VisibilityBits = __group__.bit_mask
    del __group__
    
    # Mutually Exclusive
    with FlagGroup(__bits__) as __group__: # type: FlagGroup
        AnyClass = next(__group__)
        Module   = next(__group__)
        Function = next(__group__)
        Property = next(__group__)
        _MemberTypeBits = __group__.bit_mask
    
    # Combinable
    with FlagGroup(__bits__, iter_gen=BitsCounter) as __group__: # type: FlagGroup
        RegularClass   = AnyClass | next(__group__)
        EnumClass      = AnyClass | next(__group__)
        ExceptionClass = AnyClass | next(__group__)
        AbstractClass  = AnyClass | next(__group__)
        DataClass      = AnyClass | next(__group__)
        _ClassBits     = AnyClass | __group__.bit_mask
    
    Inversed = next(__bits__)
    """ A special filter command to explicitly define an exclusive search """

IntFilter = Union[ItemFilters, int]
""" A shorter alias for `Union[ItemFilters, int]` """


T = TypeVar('T')
class MatchMode(Enum):
    """ Special enum representing `PackageInspector.detect_items()` working mode """
    
    Any = any
    """ `MatchMode.Any` represents that if an item matches any single `ItemFilters` filter, the item matches the conditions """
    
    All = all
    """ `MatchMode.All` represents that an item should match all `ItemFilters` filters to match the conditions """
    
    Non = lambda it: not any(it)
    """ `MatchMode.Non` represents that an item should match no `ItemFilters` filters to match the condition """
    
    @property
    def value(self) -> Callable[[Iterable[bool]], bool]:
        return super().value


__all__ = \
[
    'IntFilter',
    'ItemFilters',
    'MatchMode',
]

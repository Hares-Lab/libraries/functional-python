from abc import ABC
from inspect import isclass, isabstract
from typing import *

T = TypeVar('T')
Checker = Callable[[T], bool]

def is_visible_style(name: str) -> bool:
    """ Returns True for normal names, False otherwise. """
    return len(name) > 1 and name[:1] != '_'
def is_protected_style(name: str) -> bool:
    """ Returns True for _names, False otherwise. """
    return len(name) > 1 and name[:1] == '_' and name[1:2] != '_'
def is_private_style(name: str) -> bool:
    """ Returns True for __names, False otherwise. """
    return len(name) > 2 and name[:2] == '__' and name[2:3] != '_' and name[-2:] != '__'
def is_reserved_style(name: str) -> bool:
    """ Returns True for __names__, False otherwise. """
    return len(name) > 4 and name[:2] == name[-2:] == '__' and name[2:3] != '_' and name[-3:-2] != '_'

def is_instance_of(cls_or_tuple) -> Callable[[Any], bool]:
    """ `is_instance_of(cls)(obj)` is the same as `isinstance(obj, cls)` """
    return lambda obj: isinstance(obj, cls_or_tuple)
def is_subclass_of(cls_or_tuple) -> Callable[[Any], bool]:
    """ `is_subclass_of(cls)(obj)` is the same as `issubclass(obj, cls)` """
    return lambda obj: issubclass(obj, cls_or_tuple)

def is_abstract_class(cls: type) -> bool:
    """
    An *actual working* version of `inspect.isabstract` function.
    Still not possible to determine classes defined with `class C(metaclass=ABCMeta)` form.
    """
    
    return isclass(type) and (isabstract(cls) or cls is ABC or ABC in cls.__bases__)


__all__ = \
[
    'is_abstract_class',
    'is_instance_of',
    'is_private_style',
    'is_protected_style',
    'is_reserved_style',
    'is_subclass_of',
    'is_visible_style',
    
    'Checker',
]

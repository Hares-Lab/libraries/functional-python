from dataclasses import is_dataclass
from enum import Enum
from inspect import isclass, isroutine, ismodule
from typing import *

from .filter_definitions import ItemFilters
from .filter_functions import *

VISIBILITY_MATRIX: Dict[ItemFilters, Checker[str]] = \
{
    ItemFilters.Visible:    is_visible_style,
    ItemFilters.Protected:  is_protected_style,
    ItemFilters.Private:    is_private_style,
    ItemFilters.Reserved:   is_reserved_style,
}

MEMBER_TYPE_MATRIX: Dict[ItemFilters, Checker[Any]] = \
{
    ItemFilters.AnyClass:  isclass,
    ItemFilters.Module:    ismodule,
    ItemFilters.Function:  isroutine,
}

CLASS_TYPE_MATRIX: Dict[ItemFilters, Checker[type]] = \
{
    ItemFilters.EnumClass:      is_subclass_of(Enum),
    ItemFilters.ExceptionClass: is_subclass_of(Exception),
    ItemFilters.AbstractClass:  is_abstract_class,
    ItemFilters.DataClass:      is_dataclass,
}

__all__ = \
[
    'CLASS_TYPE_MATRIX',
    'MEMBER_TYPE_MATRIX',
    'VISIBILITY_MATRIX',
]

from abc import ABC
from enum import IntFlag
from functools import partial
from inspect import isclass, ismodule
from types import ModuleType
from typing import *

from cacheable_iter import lru_iter_cache

from functional import Option, Some
from .filter_definitions import *
from .filter_functions import Checker
from .filter_matrix import *

_MISSING = object()

T = TypeVar('T')
E = TypeVar('E', bound=IntFlag)
def has_flag(e: E, flag: E) -> bool:
    return e & flag == flag
def clear_flag(e: E, flag: E) -> E:
    return type(e)(e & ~flag)

Matrix = Dict[ItemFilters, Checker[T]]

class PackageInspector(ABC):
    __slots__ = ('root')
    root: ModuleType
    
    @classmethod
    @overload
    def _items_iter(cls, m: ModuleType, *, recursive: bool = False, undeclared_ok: bool = False, non_package_ok: bool = True) -> Iterator[Tuple[ModuleType, str, Any]]:
        pass
    @classmethod
    @lru_iter_cache
    def _items_iter(cls, m: ModuleType, **kwargs) -> Iterator[Tuple[ModuleType, str, Any]]:
        recursive = kwargs.get('recursive', False)
        undeclared_ok = kwargs.get('undeclared_ok', False)
        non_package_ok = kwargs.get('non_package_ok', True)
        
        keys = list()
        if (hasattr(m, '__all__')):
            if (non_package_ok):
                keys.extend(m.__all__)
            else:
                for key in m.__all__:
                    item = getattr(m, key)
                    if (not ismodule(item) and hasattr(item, '__module__') and item.__module__ != m.__name__): continue
                    keys.append(key)
        
        if (undeclared_ok or recursive):
            for key in m.__dict__.keys():
                item = getattr(m, key)
                if (ismodule(item)):
                    if (not item.__name__.startswith(f'{cls.root.__name__}.')):
                        continue
                    elif (recursive):
                        yield from cls._items_iter(item, **kwargs)
                elif (hasattr(item, '__module__') and item.__module__ != m.__name__):
                    continue
                
                if (undeclared_ok):
                    keys.append(key)
        
        for key in sorted(set(keys)):
            item = getattr(m, key)
            yield m, key, item
    
    @classmethod
    @overload
    def items_iter(cls, *, recursive: bool = False, undeclared_ok: bool = False, non_package_ok: bool = True, root_override: Option[ModuleType] = Option.empty) -> Iterator[Tuple[ModuleType, str, Any]]:
        pass
    @classmethod
    def items_iter(cls, *, root_override: Option[ModuleType] = Option.empty, **kwargs) -> Iterator[Tuple[ModuleType, str, Any]]:
        root = root_override.get_or_else(cls.root)
        yield ModuleType(name='__root__'), root.__name__, cls.root
        yield from cls._items_iter(root, **kwargs)
    
    @property
    def top_level_items(self) -> Iterator[Tuple[ModuleType, str, Any]]:
        return self.items_iter(recursive=False, undeclared_ok=False)
    
    @classmethod
    def flag_matrix(cls, item: T, filter: ItemFilters, matrix: Matrix, *, bit_mask: int, matrix_apply: Callable[[T, ItemFilters, Matrix], bool], wildcard: Option[ItemFilters], extra_condition: Option[Checker]) -> bool:
        masked = ItemFilters(bit_mask & filter)
        if (masked and extra_condition.is_empty or extra_condition == Some(True)):
            if (wildcard == Some(masked)):
                return not any(f(item) for f in matrix.values())
            else:
                return matrix_apply(item, masked, matrix)
        
        return True
    
    @classmethod
    def _exclusive_flag_matrix_apply(cls, item: T, masked: ItemFilters, matrix: Matrix) -> bool:
        return matrix[masked](item)
    @classmethod
    def exclusive_flag_matrix(cls, item: T, filter: ItemFilters, matrix: Matrix, *, bit_mask: int, wildcard: Option[ItemFilters] = Option.empty, extra_condition: Option[Checker] = Option.empty) -> bool:
        return cls.flag_matrix(item, filter, matrix, bit_mask=bit_mask, wildcard=wildcard, extra_condition=extra_condition, matrix_apply=cls._exclusive_flag_matrix_apply)
    
    @classmethod
    def _combinable_flag_matrix_apply(cls, item: T, masked: ItemFilters, matrix: Matrix) -> bool:
        for flag, func in matrix.items():
            if (has_flag(masked, flag) and not func(item)):
                return False
        else:
            return True
    @classmethod
    def combinable_flag_matrix(cls, item: T, filter: ItemFilters, matrix: Dict[ItemFilters, Checker[T]], *, bit_mask: int, wildcard: Option[ItemFilters] = Option.empty, extra_condition: Option[Checker] = Option.empty) -> bool:
        return cls.flag_matrix(item, filter, matrix, bit_mask=bit_mask, wildcard=wildcard, extra_condition=extra_condition, matrix_apply=cls._combinable_flag_matrix_apply)
    
    # noinspection PyUnusedLocal
    @classmethod
    def match_visibility(cls, key: str, item, filter: ItemFilters) -> bool:
        # noinspection PyProtectedMember
        return cls.exclusive_flag_matrix(key, filter, matrix=VISIBILITY_MATRIX, bit_mask=ItemFilters._VisibilityBits)
    
    # noinspection PyUnusedLocal
    @classmethod
    def match_member_type(cls, key: str, item, filter: ItemFilters) -> bool:
        # noinspection PyProtectedMember
        return cls.exclusive_flag_matrix(item, filter, matrix=MEMBER_TYPE_MATRIX, bit_mask=ItemFilters._MemberTypeBits, wildcard=Some(ItemFilters.Property))
    
    # noinspection PyUnusedLocal
    @classmethod
    def match_class_type(cls, key: str, item: type, filter: ItemFilters) -> bool:
        # noinspection PyProtectedMember
        return cls.combinable_flag_matrix(item, filter, matrix=CLASS_TYPE_MATRIX, bit_mask=ItemFilters._ClassBits, wildcard=Some(ItemFilters.RegularClass), extra_condition=Some(isclass(item)))
    
    @classmethod
    def _matchers(cls) -> List[Callable[[str, Any, ItemFilters], bool]]:
        return \
        [
            cls.match_visibility,
            cls.match_member_type,
            cls.match_class_type,
        ]
    
    @classmethod
    def match_filter(cls, module: ModuleType, key: str, item, filter: IntFilter):
        if (has_flag(filter, ItemFilters.Inversed)):
            return not cls.match_filter(module, key, item, clear_flag(filter, ItemFilters.Inversed))
        else:
            return all(m(key, item, filter) for m in cls._matchers())
    
    @classmethod
    @overload
    def detect_items(cls, *filters: IntFilter, match_mode: MatchMode = MatchMode.Any, recursive: bool = False, undeclared_ok: bool = False, non_package_ok: bool = True, root_override: Option[ModuleType] = Option.empty) -> Iterator[Tuple[ModuleType, str, Any]]:
        pass
    @classmethod
    @lru_iter_cache
    def detect_items(cls, *filters: IntFilter, match_mode: MatchMode = MatchMode.Any, **kwargs) -> Iterator[Tuple[ModuleType, str, Any]]:
        for data in cls.items_iter(**kwargs):
            matcher = partial(cls.match_filter, *data)
            if (match_mode.value(map(matcher, filters))):
                yield data


__all__ = \
[
    'PackageInspector',
]

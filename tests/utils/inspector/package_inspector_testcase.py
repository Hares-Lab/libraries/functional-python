from functools import lru_cache
from inspect import isabstract, isclass, ismodule, isroutine
from types import ModuleType
from typing import *
from unittest import TestCase
from unittest.util import safe_repr

from dataclasses import is_dataclass

from functional import Some, Option
from .filter_definitions import *
from .package_inspector_impl import PackageInspector
from ..decorations import make_for_decorator

_MISSING = object()

class RootContextManager(ContextManager):
    __slots__ = ('testcase', 'new_root')
    
    testcase: 'PackageInspectorTestCase'
    new_root: ModuleType
    orig_root: ModuleType
    
    def __init__(self, testcase: 'PackageInspectorTestCase', new_root: ModuleType):
        self.testcase = testcase
        self.new_root = new_root
    
    def __enter__(self):
        self.orig_root, self.testcase.root = self.testcase.root, self.new_root
        return self
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.testcase.root = self.orig_root
        return

T = TypeVar('T')
class PackageInspectorTestCase(PackageInspector, TestCase):
    @classmethod
    @lru_cache()
    def extract_checker_name(cls, checker, msg: Optional[str], name: Optional[str]) -> str:
        if (msg is None and name is None):
            if (hasattr(checker, '__name__') and checker.__name__.strip('_').startswith('is')):
                name = checker.__name__.strip('_')[2:].strip('_')
            else:
                raise ValueError("Unable to determine type name")
        
        return name
    
    def assertIsType(self, item: T, checker: Callable[[T], bool], msg: str = None, *, name: str = None):
        if (not checker(item)):
            name = self.extract_checker_name(checker, msg=msg, name=name)
            standardMsg = f"{safe_repr(item)} should be a {name}, but it is not"
            self.fail(self._formatMessage(msg, standardMsg))
    
    def assertIsNotType(self, item: T, checker: Callable[[T], bool], msg: str = None, *, name: str = None):
        if (checker(item)):
            name = self.extract_checker_name(checker, msg=msg, name=name)
            standardMsg = f"{safe_repr(item)} should not be a {name}, but it is"
            self.fail(self._formatMessage(msg, standardMsg))
    
    def assertIsSubclass(self, obj, cls, msg=None):
        """ Same as self.assertTrue(issubclass(obj, cls)), with a nicer default message. """
        if (not issubclass(obj, cls)):
            standardMsg = '%s is not an instance of %r' % (safe_repr(obj), cls)
            self.fail(self._formatMessage(msg, standardMsg))
    
    def assertNotIsSubclass(self, obj, cls, msg=None):
        """ Included for symmetry with assertIsSubclass. """
        if (issubclass(obj, cls)):
            standardMsg = '%s is an instance of %r' % (safe_repr(obj), cls)
            self.fail(self._formatMessage(msg, standardMsg))
    
    assertIsDataClass      = lambda self, item, *args, **kwargs: self.assertIsType(item, is_dataclass)
    assertIsAbstractClass  = lambda self, item, *args, **kwargs: self.assertIsType(item, isabstract)
    assertIsClass          = lambda self, item, *args, **kwargs: self.assertIsType(item, isclass)
    assertIsFunction       = lambda self, item, *args, **kwargs: self.assertIsType(item, isroutine)
    assertIsModule         = lambda self, item, *args, **kwargs: self.assertIsType(item, ismodule)
    
    assertIsNotDataClass      = lambda self, item, *args, **kwargs: self.assertIsNotType(item, is_dataclass)
    assertIsNotAbstractClass  = lambda self, item, *args, **kwargs: self.assertIsNotType(item, isabstract)
    assertIsNotClass          = lambda self, item, *args, **kwargs: self.assertIsNotType(item, isclass)
    assertIsNotFunction       = lambda self, item, *args, **kwargs: self.assertIsNotType(item, isroutine)
    assertIsNotModule         = lambda self, item, *args, **kwargs: self.assertIsNotType(item, ismodule)
    
    def newRoot(self, new_root: ModuleType) -> RootContextManager:
        return RootContextManager(self, new_root)
    
    @overload
    def detect_items(self, *filters: IntFilter, match_mode: MatchMode = MatchMode.Any, recursive: bool = False, undeclared_ok: bool = False, non_package_ok: bool = True, root_override: Option[ModuleType] = Option.empty) -> Iterator[Tuple[ModuleType, str, Any]]:
        pass
    def detect_items(self, *filters: IntFilter, **kwargs) -> Iterator[Tuple[ModuleType, str, Any]]:
        kwargs.setdefault('root_override', Some(self.root))
        return super().detect_items(*filters, **kwargs)

@overload
def discover_items(*filters: IntFilter, match_mode: MatchMode = MatchMode.Any, recursive: bool = False, undeclared_ok: bool = False, non_package_ok: bool = True, root_override: Option[ModuleType] = Option.empty):
    pass
def discover_items(*filters: IntFilter, **kwargs):
    # noinspection PyTypeChecker
    decorator = make_for_decorator \
    (
        iter_func = lambda self: self.detect_items(*filters, **kwargs),
        subtest_keys_func = lambda module, key, item: dict(key=f'{module.__name__}.{key}'),
    )
    
    return decorator


__all__ = \
[
    'discover_items',
    
    'PackageInspectorTestCase',
    'RootContextManager',
]

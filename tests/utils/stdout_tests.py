import io
import sys
from typing import ContextManager


class OutputContextManager(ContextManager):
    def __init__(self):
        self.stdout = io.StringIO()
        self.stderr = io.StringIO()
    
    def __enter__(self):
        self._system_stdout = sys.stdout
        self._system_stderr = sys.stderr
        
        sys.stdout = self.stdout
        sys.stderr = self.stderr
        
        return self
    
    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout = self._system_stdout
        sys.stderr = self._system_stderr
        
        if (exc_type): raise
        return None


__all__ = \
[
    'OutputContextManager',
]
